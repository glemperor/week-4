import 'package:flutter/material.dart';

class Category {
  Category({required this.name, required this.image});
  String name;
  ImageProvider image;
}

class UtilityService {
  UtilityService({required this.name, required this.image});
  String name;
  ImageProvider image;
}

class Product {
  Product({
    required this.name,
    required this.image,
    required this.salePrice,
    required this.regularPrice,
    required this.rating,
    required this.reviewCount,
    required this.badges
  });
  String name;
  ImageProvider image;
  int salePrice;
  int regularPrice;
  double rating;
  int reviewCount;
  List badges;
}