import 'package:flutter/material.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:week4/home/home_app_bar.dart';
import 'package:week4/widget/box.dart';
import 'package:week4/mock_data.dart' as mockData;
class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            Container(
              height: 24.0,
              color: Colors.blue[700],
            ),
            Container(
              color: Colors.blue,
              child: ListTile(
                leading: Icon(Icons.account_circle, size: 40.0,),
                title: Text(
                  "Nguyễn Minh Trí", style: TextStyle(color: Colors.white),),
                subtitle: Text(
                  "trigpm@gmail.com", style: TextStyle(color: Colors.white),),
              ),
            ),
            ListTile(
              selected: true,
              leading: Icon(Icons.home),
              title: Text("Trang chủ"),
            ),
            ListTile(
              leading: Icon(Icons.view_list),
              title: Text("Danh sách ngành hàng"),
            ),
            ListTile(
              leading: Icon(Icons.receipt),
              title: Text("Quản lý đơn hàng"),
            ),
            ListTile(
              leading: Icon(Icons.favorite),
              title: Text("Sản phẩm yêu thích"),
            ),
            ListTile(
              leading: Icon(Icons.account_circle),
              title: Text("Quản lý tài khoản"),
            ),
            ListTile(
              leading: Icon(Icons.notifications),
              title: Text("Thông báo của tôi"),
            )
          ],),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverHomeAppBar(onMenuIconPressed: () {
            _scaffoldKey.currentState!.openDrawer();
          },),
          SliverToBoxAdapter(
            child: Box(
              title: Text("   Danh sách ngành hàng"),
              action: FlatButton(onPressed: () {}, child: Text("XEM TẤT CẢ")),
              child: GridView.count(
                crossAxisCount: 2,
                scrollDirection: Axis.horizontal,
                childAspectRatio: 1,
                children: buildCategories(),
              ),
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate([
                Box(
                  title: Text("Danh sách ngành hàng"),
                  action: RaisedButton(onPressed: (){}, child: Text("XEM TẤT CẢ"), color: Colors.blue,),
                  child: Text("Content"),

                )
              ]),
          ),
          SliverFixedExtentList(
              itemExtent: 56.0,
              delegate: SliverChildBuilderDelegate((context, index) {
                return Container(color: Colors.red[(index % 9) * 100],);
              })),
        ],
      ),
    );
  }

  List<Widget> buildCategories() {
    return mockData.categories.map((category) {
      return Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Image(image: category.image, fit: BoxFit.cover,),
            Text(category.name)
          ],
        ),
      );
    }).toList();
  }
}

