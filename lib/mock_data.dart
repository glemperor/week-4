import 'package:flutter/cupertino.dart';
import 'package:week4/model.dart';

import 'main.dart';

List<UtilityService> utilityServices = [
  new UtilityService(name:  'Vé máy bay', image: AssetImage('')),
  new UtilityService(name:  'Mua bảo hiểm online', image: AssetImage('')),
  new UtilityService(name:  'Mua thẻ điện thoại', image: AssetImage('')),
  new UtilityService(name:  'Thẻ game', image: AssetImage('')),
  new UtilityService(name:  'Nạp tiền điện thoại', image: AssetImage('')),
  new UtilityService(name:  'Thẻ 3G/4G', image: AssetImage(''))
];

List<Category> categories = [
  new Category(name: 'Nhà sách Tiki', image: AssetImage('assets/img/categories/nha_sach_tiki.jpg')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),
  new Category(name: 'Nhà sách Tiki', image: AssetImage('')),

];